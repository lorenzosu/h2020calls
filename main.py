from bottle import Bottle
import calendarparser

bottle = Bottle()

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.


@bottle.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return("Go to <a href='./h2020/calls'>H2020 next calls</a>")

@bottle.route('/h2020/calls')
def hello():
    """Return a friendly HTTP greeting."""
    cal = calendarparser.CallCalendar()
    cal.parse_file()
    ret_html = cal.to_html()
    #print ret_html
    return ret_html
    #return "test"



# Define an handler for 404 errors.
@bottle.error(404)
def error_404(error):
    """Return a custom 404 error."""
    return 'Sorry, nothing at this URL.'
