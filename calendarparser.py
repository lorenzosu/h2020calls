import pprint
import urllib2
from lxml import etree
from string import Template
from datetime import datetime
from collections import defaultdict

MONTH_DIC = {
        "Jan": "01",
        "Feb": "02",
        "Mar": "03",
        "Apr": "04",
        "May": "05",
        "Jun": "06",
        "Jul": "07",
        "Aug": "08",
        "Sep": "09",
        "Oct": "10",
        "Nov": "11",
        "Dec": "12"
        }


class CallEvent:
    def __init__(self):
        self.event_dic = {}

    def to_string(self):
        event_string = ''
        for k in self.event_dic.keys():
            event_string = event_string + k + ':' + self.event_dic[k] + "\n"
        return event_string

    def printer(self):
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(self.event_dic)

class CallCalendar:
    def __init__(self):
        """ Initialise the object and create a list for all events """
        self.download_ok = False
        self.cal_url="http://ec.europa.eu/research/participants/portal/data/call/h2020/calls.ics"
        self.date_from = datetime.now()
        self.date_to=datetime(2020,12,31,0,0)
        self.event_list = []
        # A dictionary of deadline dates, events
        self.date_dic = defaultdict(list)

    def parse_file(self):
        """ Download and parse the H2020 ical file in path, and create a CallEvent object for
        each event detected. Append each event to the global event list """
        this_event = None
        try:
            print "Downloading ics calendar from H2020 website..."
            response = urllib2.urlopen(self.cal_url)
            # Just in case... we keep everything in this try. 
            for line in response:
                #print line
                if line.find("BEGIN:VEVENT") >= 0:
                    this_event = CallEvent()
                    continue
                if line.find("END:VEVENT") >= 0:
                    self.event_list.append(this_event)
                    continue
                if this_event == None:
                    continue
                else:
                    attrib, data = line.split(':',1)
                    this_event.event_dic[attrib] = data
            self.clean_events()
            self.date_filter()
            
            self.download_ok = True

        except:
            # TODO make it cleaner
            print("Something went wrong...")
            self.download_ok = False
            return -1

    def clean_events(self):
        """ After parsing 'clean' the events (e.g. make uniform deadlines etc. """
        for e in self.event_list:
            edic = e.event_dic
            description = edic['DESCRIPTION']
            description = description.split('\\n')
            try:
                deadline_date = edic['DTSTART']
            except:
                deadline_date = edic['DTSTART;VALUE=DATE']
            for l in description:
                if l.find("Deadline:") >= 0:
                    date_string = l[l.find(",",1) + 1:]

                    date_string = date_string[0:date_string.find(":",1) -2]
                    date_string = date_string
                    date_string = date_string.strip()
                    
                    day, month, year = date_string.split(" ")
                    month = MONTH_DIC[month]
                    if len(day) < 2:
                        day = '0' + day
                    deadline_date = year + month + day
            deadline_string = (
                    deadline_date[6:8] + " / " +
                    deadline_date[4:6] + " / " +
                    deadline_date[0:4]
                    )
 
            link = description[-1]
            l, url = link.split(":",1)
            link_line = "Link: " + "<a href='" + url +"'>"+url+"</a>"
            description = description[0:-1]
            description.append(link_line)
            description = '<br />'.join(description)
            description = description.replace("\\","")

            edic['DESCRIPTION'] = description
            e.deadline = deadline_date
            e.call_url = url
            #print e.deadline
            edic['deadline_string'] = deadline_string

    def date_filter(self):
        """ Sort by date and select only calls from date_from (default today)
        up until date_to (default 31 Dec 2020)"""
        for e in self.event_list:
            this_date = datetime.strptime(e.deadline, "%Y%m%d")
            if (this_date >= self.date_from) and (this_date < self.date_to):
                #print("----- " + e.deadline)
                self.date_dic[e.deadline].append(e)
            else:
                #print("## Not in date range  "+e.deadline)
                pass

    def call_page_parser(self, url):
        """ Parse the horrible html of each call page and extract topic urls """
        call_list = []
        try:
            r = urllib2.urlopen(url).read()
        except urllib2.HTTPError as e:
            print "HTTP error downloading " + url
            call_list.append( ('Above call URL broken on H2020 page!!!','#') )
            return call_list
        html = etree.HTML(r)
        url_el_list = html.xpath('//div[@id="tab1"]/ul/li/a')
        for u in url_el_list:
            text = u.text
            full_url = url.rsplit('/',2)[0] + "/" + u.attrib['href'].split('/',1)[1]
            call_list.append( (text, full_url) )
        return call_list

    def printer(self):
        for e in self.event_list:
            e.printer()

    def to_html(self):
        if not self.download_ok:
            return "ERROR downloading calendar file from H2020 website."

        """ generate html """
        html = """<head>
    <title>Calls</title>
    <style type="text/css">
    h1 {
    	font-family: Sans-serif;
        font-size: large;
        }
    </style>
</head>
<body>
<div>
"""
        header_string = ("<h1>Next H2020 Calls from <u>" +
            self.date_from.strftime("%d %b %Y") + "</u> to <u>" +
            self.date_to.strftime("%d %b %Y") +
            "</u></h1></div>"
            )
        html += header_string
        html += "<div><p>As published in H2020 calendar available on: <a href='http://ec.europa.eu/research/participants/portal/desktop/en/opportunities/'>H2020 Participant Portal</a>"
        for k in sorted(self.date_dic.keys()):
            for e in self.date_dic[k]:
                edic = e.event_dic
                call_string = (
                        "<div class='call_div'>\n\t<h1 class='h1' style='font:sans'>" +
                        edic['SUMMARY'].replace("\\","") + 
                        "</h1>\n\t<p><em>Deadline Date: </em>" +
                        edic['deadline_string'] + "\n\t</p>\n" +
                        "\t<p>" + edic['DESCRIPTION'] + "</p>\n"
                    )
                
                html = html + call_string

                print "Fetching topics for call at: " + e.call_url
                topic_list = self.call_page_parser(e.call_url)
                html += "<ul>"
                for t in topic_list:
                    line = "<li>""<a href='"+ t[1].strip() + "'>" + t[0].encode('utf-8') + "</a></li>"
                    print line
                    html += line

                html += "</ul>"
                html += "</div>\n"
            html += "</body>\n</html>"
        return html

""" test """ 
cal = CallCalendar()
cal.parse_file()
ret_html = cal.to_html()
#cal.printer()
#print ret_html

